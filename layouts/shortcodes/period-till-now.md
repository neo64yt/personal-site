{{ $currentYear := now.Format "2006" | int }}
{{ $refYear := index .Params 0 | int }}
{{ sub $currentYear $refYear }}
