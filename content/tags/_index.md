---
cascade:
    - build:
        list: always
        publishResources: true
        render: always

build:
    list: never
    publishResources: false
    render: never
---
