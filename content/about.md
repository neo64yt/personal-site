---
title: "👤 Who Am I?"
layout: about
---
![Neo64](/img/neo64-new.jpg)

## About Myself
Some Internet entity who is believed to be from Malaysia. Also known as `dev_sda2`. 

I am a casual Internet user with several interests who is also a Linux user for about {{< period-till-now 2020 >}} years. Of course, this site will be towards that (excluding whatever other things are available here). So, this site might be the right place for you if you have the same interest! 

I own a [YouTube channel](https://youtube.com/@neo64ishere) and an [Odysee channel](https://odysee.com/@Neo64IsHere:1) where I post my videos without any publish schedule. As I'm still looking for ample free time to **actually** create new videos, please be patient. While waiting, why don't watch my old ~~cringy~~ videos first? ^_^

## Un-interesting facts
+ Despite how it can be seen that way, no, Neo64 is not inspired by Nintendo 64 (sorry Nintendo sisters).
+ My official profile picture above was originally drawn using coloured erasers in MS Paint.
+ Most of my efforts I put on this website are probably in the design. Expect more design changes instead of quality content.
+ I use Artix btw.
+ Sample text 

## Appearance
Apart from the platforms listed on the homepage, I may appear on other platforms with "neo64" in my usernames or handles. However, I AM NOT and WON'T be on these platforms (at least not officially as Neo64):
+ Any Meta platforms
+ TikTok
+ Xitter
+ Reddit

And oh, these Neo64-s below are not me. I will update this list everytime I find more.
+ Neo64 on Minecraft Java (this player exists long before I call myself Neo64)

## Contact
Feel free to contact me through these platforms:
+ Email: neo64@disroot.org 
+ Matrix: @neo64yt:matrix.org
+ Discord: @darealneo64
