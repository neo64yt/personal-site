---
title: "Software"
linkTitle: "sw"
---
Software that I use, mostly FOSS.

### -> [Artix Linux](https://artixlinux.org/)
The Linux distro with both the benefits of Arch and systemd-free ecosystem. Also apolitical, if you want that, as they actually focus on Linux (like any distro should be) instead of whatever annoying Shit Nobody Cares About (SNCA) activism the Westerners are pushing. This is what I'm currently daily driving btw!

### -> [mpv](https://mpv.io)
A simple media player for CLI users that is as powerful as VLC.

### -> [Firefox](https://www.mozilla.org/en-US/firefox/)
Not really the best web browser, but it definitely is the current best alternative to Chrome that isn't Blink-based. Harden it with [arkenfox user.js](https://github.com/arkenfox/user.js/) and it's pretty great for privacy. 

### -> [Hugo](https://gohugo.io)
The static site generator that does most of the heavylifting of gluing together the pieces of my website.

### -> [Neovim](https://neovim.io)
A pretty solid modern modal text editor. Pretty extensible too, especially with Lua support.

### -> [Droid-ify](https://f-droid.org/en/packages/com.looker.droidify/)
A minimalistic Material You F-Droid client that does it job well. Also includes third-party repos by default as well.

### -> [Revanced](https://revanced.app)
I can't even imagine using YouTube on mobile without this anymore.
