---
title: "Jaringan Sejagat"
linkTitle: "www"
---
Pautan berkaitan dengan Jaringan Sejagat, terutamanya dari sisi indie.

#### Petunjuk:
+ {{< legend-icon "material-warning-red.png" "[NSFW]" >}} - Pautan laman web (berkemungkinan) mengandungi kandungan NSFW

### -> {{< 88x31 "https://neocities.org" "neocitiesorg.gif" "Neocities" >}}
Senang kata, Geocities zaman moden. Neocities pun _host_ pertama untuk _website_ ni sebelum Github & Codeberg.

### -> [ASCII Art Archive](https://www.asciiart.eu/)
Koleksi besar lukisan seni ASCII & gambar seni ASCII lain yang berkaitan. Aku ambik lukisan seni ASCII yang aku guna dalam _banner_ _website_ ni kat sini!

### -> {{< legend-icon "material-warning-red.png" "[NSFW]" >}} {{< 88x31 "https://capstasher.neocities.org/88x31collection-page1" "largest-88x31.png" "The Largest 88x31 Collection on The Internet" >}} ([archive](https://web.archive.org/web/20241004223620/https://capstasher.neocities.org/88x31collection-page1))
Koleksi _button_ 88x31 dengan 14 _page_ oleh [capstasher](https://capstasher.neocities.org/)!

### -> [Free Animated Horizontal Line GIFs](https://gifs.cc/animated-lines-2.shtml)
Aku dapat satu (yang ada kat _homepage_) kat sini. Kena la bagi kredit untuk _website_ ni.
