---
title: "The World Wide Web"
linkTitle: "www"
---
Links related to the World Wide Web, especially from the indie side of the Web.

#### Legend:
+ {{< legend-icon "material-warning-red.png" "[NSFW]" >}} - The linked website (potentially) contains NSFW content

### -> {{< 88x31 "https://neocities.org" "neocitiesorg.gif" "Neocities" >}}
Basically the Geocities of the modern day. Neocities was also the first host of my website before Github and now, Codeberg. 

### -> [ASCII Art Archive](https://www.asciiart.eu/)
A large collection of ASCII art drawings and other related ASCII art pictures. I get some of the ASCII art drawings that I use in my banners from here!

### -> {{< legend-icon "material-warning-red.png" "[NSFW]" >}} {{< 88x31 "https://capstasher.neocities.org/88x31collection-page1" "largest-88x31.png" "The Largest 88x31 Collection on The Internet" >}} ([archive](https://web.archive.org/web/20241004223620/https://capstasher.neocities.org/88x31collection-page1))
A 88x31 button collection with 14 pages of them by [capstasher](https://capstasher.neocities.org/)!

### -> [Free Animated Horizontal Line GIFs](https://gifs.cc/animated-lines-2.shtml)
I got one (in the homepage) from here. Gotta credit the website here.
