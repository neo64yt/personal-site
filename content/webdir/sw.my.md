---
title: "Perisian"
linkTitle: "sw"
---
Perisian yang aku guna yang kebanyakannya FOSS.

### -> [Artix Linux](https://artixlinux.org/)
_Distro_ Linux dengan faedah daripada dua-dua Arch dan ekosistem _systemd-free_. Kalau nak _distro_ yang bebas politik, boleh kata yang ni sesuai sebab betul-betul fokus pasal Linux (macam yang sepatutnya semua _distro_ buat) dan tak sibuk dengan apa-apa aktivisme dari Barat yang serabut dan takde orang kisah sangat. Ni lah yang aku tengah _daily drive_ sekarang!

### -> [mpv](https://mpv.io)
Pemain media ringkas untuk pengguna CLI yang kecil-kecil cili padi setaraf dengan VLC.

### -> [Firefox](https://www.mozilla.org/en-US/firefox/)
Bukan _web browser_ yang terbaik, tapi semestinya alternatif Chrome terbaik yang bukan _Blink-based_. _Harden_ dengan [arkenfox user.js](https://github.com/arkenfox/user.js/), cukup elok untuk privasi. 

### -> [Hugo](https://gohugo.io)
Generator _site_ statik yang buat semua kerja pasang semua bahagian _website_ aku.

### -> [Neovim](https://neovim.io)
_Text editor_ bermod yang moden yang cukup baik. Sangat _extensible_, lagi-lagi dengan Lua.

### -> [Droid-ify](https://f-droid.org/en/packages/com.looker.droidify/)
_Client_ F-Droid minimalistik bertema Material You yang yang buat kerja dengan molek. Ada tersedia jugak _third-party repo_ lepas mula-mula _install_.

### -> [Revanced](https://revanced.app)
Dah tak boleh bayang tengok YouTube kat _phone_ dengan takde Revanced.
