---
title: "📃 Blog"
layout: blog
---

Ni indeks untuk semua artikel blog yang aku tulis. Aku takde jadual untuk buat _post_ baru, jadi silakan _subscribe_ _RSS feed_ aku untuk tak terlepas artikel-artikel baru aku (kalau ada :P).

Semua artikel yang aku tulis ni pendapat peribadi aku, jadi mintak jangan percaya bulat-bulat. Kalau ada apa-apa yang aku silap dalam mana-mana artikel dan ada nak betulkan, sila _reply_ dengan email (aku lagi suka kalau email tu siap _encrypt_ dengan PGP). 
