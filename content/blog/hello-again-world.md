---
title: "Hello Again, World"
date: "2023-09-01T22:19:43+08:00"
publishdate: "2023-09-01"
tags:
- updates
- crap
draft: false
---
![Hello World GIF](https://media.tenor.com/mGgWY8RkgYMAAAAC/hello-world.gif)

After delaying the development of this website for a long time, finally on this date (2023-09-01), `neo64yt.codeberg.page` is up. This means that my old site, `neo64yt.github.io` is now officially obsolete. I'm really satisfied that my brand new website, which took me a long time to make (especially that 2023 is a busy year for me) is now online.

However, getting it online is just the first step. There's still **a lot** on this website that I need to finish (you can see that I don't even have post tagging set up yet). Anyway, I hope you enjoy this cleaner and more featureful version of my website, compared to the old one. :) 

This would be my first and last article in this year. So, see you again in 2024.

Oh, don't forget to subscribe to my [RSS feed](/index.xml).

