---
title: "The GSI Experience"
date: "2025-02-01T12:13:17+08:00"
tags:
- android
- tech
draft: false
---
Finally, an ACTUAL post!

Being a person who likes to tinker with things, including phones, I love rooting and custom ROMs. I remember how I started involving myself into this scene with "how can I install Android 7 on this phone?", which was a Samsung Galaxy Note II (GT-N7100) with Android Kitkat. Oh, this was also (indirectly) the beginning of my Linux journey. You might not be reading this if I didn't have that question in my head XD.

Fast forward to around 2021, I bought a Samsung Galaxy A22 5G (SM-A226B), which I'm still using during the time I write this. At the time, I already a bit deep down into the online privacy rabbithole, so of course I tried to deGoogle and deSamsung the phone. And that's when I realized that my choice of buying this phone was a mistake - it uses a MediaTek chipset instead of a Snapdragon or an Exynos. Due to how MediaTek doesn't release the kernel source for their chips, it's hard for devs to port custom ROMs to MediaTek devices. And since this is a Samsung, of course it's even harder to port one. I'm considering a Snapdragon-powered Xiaomi for my next phone...

Back on the topic, even though there are no custom ROMs optimized for this phone, there is another way to run a custom ROM on such devices. Thanks to [Project Treble](https://android-developers.googleblog.com/2017/05/here-comes-treble-modular-base-for.html) that has been here since Android Oreo and the massive amount of work done by [TrebleDroid](https://github.com/TrebleDroid) contributors, we can at least try to run AOSP forks (or even just pure AOSP) on these devices using Generic System Images, or for short, GSIs. It's not perfect as it obviously isn't optimized for all Android-capable devices as implied by the "generic" in the name, but hey, the option is there.

![A simplification of how Project Treble works](https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEiFS85Hz74dWvP-HO3kIa5VCSXZqlOn_EqxBD1dPiGFa3ShKqalQUne3mh4TII6tR_6II4RV1hDwyf4ZEbSASYC467cZaGPe4NVP5xyx0qpo8-JLHR5xYDiTfvbLHuWwJsQ6jR6HIVb17n9/s640/image2.png)

Now, to the title. So far, for about two years of daily driving two GSIs, crDroid 9 and LineageOS 20 (both are based on Android 13), I can say that they are usable and daily drivable on my phone. Everything works out of the box after installation except for SMS, 5G, VoLTE and MTP. These features (SMS and 5G) can be easily fixed with a little bit of tweaking though, thanks to the community in the [A22 5G forum on XDA](https://xdaforums.com/f/samsung-galaxy-a22-5g.12335/). That's all I have to say about the usability.

In terms of performance though, they are a bit frustrating. As I mentioned above, GSIs are not optimized, so it is expected that there are performance issues. The most notable one for me is that GSIs drain more battery power compared to the stock OneUI Core. I have to charge my phone more often due to this. The other notable one for me is that they tend to be sluggish at times (though to be fair, I also experience this issue on OneUI). 

For the Experience™ itself, of course they feel lighter compared to OneUI as they contain less bloat, as you would expect from a custom ROM. Apart from that, these GSIs are more customizable than OneUI - as in customizable without the need of themes from Galaxy Store. While the customizability are different for each GSI variant as inherited from their respective parent projects, they do share a few customization options (including fonts, icon shape and icon pack) which even exist in the pure AOSP variant, being TrebleDroid-based GSIs.

So, that's all for this first actual post. Starting from this one, I hope I could spend more time to write more actual and longer articles in this year. Please let me know what you think about one and what you want to see next. :D 
