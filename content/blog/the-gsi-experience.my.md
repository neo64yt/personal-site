---
title: "Pengalaman dengan GSI"
date: "2025-02-01T12:13:17+08:00"
tags:
- android
- tech
draft: false
---
Akhirnya, ada jugok _post_ hok betul-betul!

Sebagai orang yang jenis suka main-main dengan macam-macam benda, termasuk telefon, aku suka _rooting_ dan _custom ROM_. Aku ingat lagi macam mana aku mula minat benda ni dengan "guane nok _upgrade_ tepon ni jadi Android 7?", yang merupakan sebuah Samsung Galaxy Note II (GT-N7100) dengan Android Kitkat. Oh, ni jugak la titik mula aku dengan Linux (secara tak langsung). Rasanya memang takde la artikel ni kalau aku takde soalan ni dalam kepala dulu XD.

Berganjak ke tahun 2021, aku beli sebuah Samsung Galaxy A22 5G (SM-A226B), yang aku masih guna masa menulis artikel ni. Masa tu, aku dah terjebak sikit dengan bab-bab _online privacy_, jadi ada la aku cuba untuk _deGoogle_ dan _deSamsung_ telefon ni. Masa ni la aku tersedar aku dah buat silap pilih telefon ni. Telefon ni guna cip MediaTek, bukannya Snapdragon atau Exynos. Atas sebab MediaTek tak bagi _source_ untuk _kernel_ untuk cip-cip diorang, susah untuk _developer_ nak _port_ _custom ROM_ untuk _device_ dengan cip MediaTek. Atas sebab telefon ni berjenama Samsung jugak, makin susah nak buat kerja _porting_. Dalam kepala aku, memang Xiaomi dengan cip Snapdragon la aku beli lepas ni...

Balik ke topik, walaupun tak ada _custom ROM_ yang dah _optimized_ untuk telefon ni, ada satu lagi cara untuk guna _custom ROM_ kat telefon ni dan mana-mana _device_ yang ada masalah sama. Dengan adanya [_Project Treble_](https://android-developers.googleblog.com/2017/05/here-comes-treble-modular-base-for.html) yang dah wujud sejak Android Oreo dan jasa-jasa _contributor_ projek [TrebleDroid](https://github.com/TrebleDroid) yang tak terkira, sekurang-kurangnya kita boleh cuba untuk _run_ _fork_ AOSP (AOSP asli pun boleh) kat _device_ macam ni dengan _Generic System Image_, atau pendeknya, GSI. Memang dah jelas-jelas tak sempurna atas sebab tak _optimized_ untuk semua _device_ Android dengan ada "_generic_" dalam nama, tapi dah kira OK lah ada pilihan untuk guna _custom ROM_.

![Ringkasan macam mana _Project Treble_ berfungsi](https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEiFS85Hz74dWvP-HO3kIa5VCSXZqlOn_EqxBD1dPiGFa3ShKqalQUne3mh4TII6tR_6II4RV1hDwyf4ZEbSASYC467cZaGPe4NVP5xyx0qpo8-JLHR5xYDiTfvbLHuWwJsQ6jR6HIVb17n9/s640/image2.png)

Sekarang, sampai kat bahagian dalam tajuk. Setakat ni, selama dua tahun guna dua GSI sebagai _daily driver_, crDroid 9 dan LineageOS 20 (dua-dua Android 13), aku boleh la kat boleh guna dan _daily drivable_ kat telefon aku. Semua benda berfungsi _out of the box_ lepas _install_ kecuali SMS, 5G, VoLTE dan MTP. Walau macam manapun, senang je nak baiki fungsi-fungsi ni (SMS dan 5G) dengan pertologan komuniti dalam [forum A22 5G kat XDA](https://xdaforums.com/f/samsung-galaxy-a22-5g.12335/). Sampai situ apa yang aku nak kat pasal _usability_ GSI.

Dari segi _performance_ pulak, sakit kepala sikit. Macam aku kata kat atas, GSI tak _optimized_, jadi memang tak pelik ada isu _performance_. Yang paling jelas bagi aku, GSI lagi kuat makan bateri kalau nak dibandingkan dengan OneUI Core yang _stock_. Kena cas banyak kali telefon ni atas sebab ni. Selain tu, kadang-kadang ada rasa _sluggish_ (tapi OneUI pun macam ni jugak). 

Untuk "pengalaman" tu pulak, semestinya GSI rasa lagi ringan dibandingkan dengan OneUI sebab kurang _bloat_, macam yang biasanya kita _expect_ daripada _custom ROM_. Selain tu, GSI lagi _customizable_ daripada OneUI, tanpa perlua _install_ _theme_ daripada Galaxy Store. Walaupun _customizability_ lain-lain untuk setiap _variant_ GSI, diwarisi daripada projek utama masing-masing, ada persamaan untuk beberapa _option_ _customization_ (termasuk _font_, bentuk ikon dan pek ikon) yang jugak wujud dalam _variant_ AOSP asli, untuk GSI bawah TrebleDroid.

Jadi, sampai sini je la betul-betul _post_ yang pertama. Mula dengan yang ni, aku harap boleh la aku guna lagi banyak masa untuk tulis lagi banyak artikel yang lagi panjang tahun ni. Bagitahu la ye pendapat pasal artikel ni dan nak baca apa lain kali. :D 
