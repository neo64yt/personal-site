---
title: "Hai Sekali Lagi, Dunia"
date: "2023-09-01T22:19:43+08:00"
publishdate: "2023-09-01"
tags:
- updates
- crap
draft: false
---
![Hello World GIF](https://media.tenor.com/mGgWY8RkgYMAAAAC/hello-world.gif)

Lepas sekian lama tangguh _development_ untuk _website_ ni, akhirnya pada tarikh ni (2023-09-01), `neo64yt.codeberg.page` dah bangun. Jadi, _website_ lama aku sebelum ni, `neo64yt.github.io` secara rasminya dah tak boleh pakai. Aku sangat puas hati yang _website_ baru aku ni, yang ambil masa lama nak buat (lagi-lagi dengan 2023 jadi tahun sibuk untuk aku) dah _online_.

Tapi, buat _website_ ni _online_ baru saja langkah pertama. **Banyak** lagi dalam _website_ ni yang aku kena siapkan (_tagging_ untuk _post_ pun aku tak _set up_ lagi). Apa-apa pun, aku harap semua pelawat suka versi _website_ ni yang lagi bersih dan banyak _feature_, kalau nak bandingkan dengan yang lama. :) 

Artikel ni akan jadi yang pertama dan terakhir yang aku tulis untuk tahun ni. Jadi, jumpa lagi dalam 2024 nanti.

Oh, jangan lupa _subscribe_ [RSS](/index.xml) aku.

