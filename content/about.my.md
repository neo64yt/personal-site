---
title: "👤 Siapa Aku?"
layout: about
---
![Neo64](/img/neo64-new.jpg)

## Tentang Aku
Entiti Internet yang dipercayai berasal daripada Malaysia. Juga dikenali sebagai `dev_sda2`.

Aku ni pengguna Internet yang ada beberapa minat dan dah guna Linux selama {{< period-till-now 2020 >}} tahun. Sebab tu, _website_ ni semestinya lagi ke arah tu (tak termasuk apa-apa selain yang ada aku kata kat _website_ ni). Jadi, _website_ ni mungkin sesuai untuk yang ada minat yang sama!   

Aku ada [_channel_ YouTube](https://youtube.com/@neo64ishere) dan [Odysee](https://odysee.com/@Neo64IsHere:1) kat mana aku _upload_ video-video aku. Aku tengah mencari-cari masa lapang yang cukup untuk buat video. Jadi, harap bersabar. Sambil-sambil tu, silakan tonton video-video ~~_cringe_~~ lama aku ^_^. 

## Fakta tak menarik
+ Walaupun nampak macam tu, tak, Neo64 asalnya bukan daripada Nintendo 64 (maaf ye geng Nintendo)
+ Gambar profil rasmi aku kat atas asalnya dilukis dengan pemadam berwarna MS Paint.
+ Usaha aku buat _website_ ni kemungkinan banyak dalam buat _design_. Jangan hairan kenapa lagi banyak perubahan _design_ daripada konten.
+ Aku guna Artix btw.
+ Sample text

## Penampakan
Selain platform-platform yang ada kat _homepage_, aku mungkin akan muncul kat platform lain dengan "neo64" dalam _username_. Tapi, aku TAK ADA dan TAK AKAN ADA kat platform-platform ni (sekurang-kurangnya bukan secara rasmi dengan nama Neo64):
+ Mana-mana platform Meta
+ TikTok
+ Xitter
+ Reddit

Oh, Neo64 kat bawah ni bukan aku ye. Aku akan panjangkan lagi senarai ni bila jumpa banyak lagi.
+ Neo64 kat Minecraft Java (dia dah guna _username_ ni lama sebelum aku guna nama Neo64)

## Berhubung
Untuk berhubung, guna platform-platform kat bawah:
+ Email: neo64@disroot.org
+ Matrix: @neo64yt:matrix.org
+ Discord: @darealneo64


