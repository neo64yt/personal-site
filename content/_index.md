---
title: "🏠 Home"
---
![Neo64's Logo](/img/neo64-new.jpg)

Welcome to `neo64yt.codeberg.page`, my low budget personal website. As you can see here, this website is currently a little bit incomplete and there's nothing much you can see here. However, feel free to look around here while I'm slowly developing this website :)

